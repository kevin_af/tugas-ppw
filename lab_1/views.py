from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Kevina Felie Arihadi'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 4, 5)
npm = 1906298973 
major = 'Information Systems'
uni = 'University of Indonesia'
angkatan = 'Maung 2019'


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'major': major, 'uni': uni, 'angkatan': angkatan}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
